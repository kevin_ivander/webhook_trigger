# frozen_string_literal: true

module WebhookTrigger
  # This Configuration class is for initialize config to database
  class Configuration
    attr_accessor :db, :host, :port, :username, :password, :buffer_table,
                  :list, :event

    def initialize
      @db = nil
      @host = nil
      @port = nil
      @username = nil
      @password = nil
      @buffer_table = nil
      @list = nil
      @event = nil
    end
  end
end
