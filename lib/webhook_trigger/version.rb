# frozen_string_literal: true

require 'webhook_trigger/configuration.rb'
# Module WebhookTrigger is main module for gem webhook_trigger
module WebhookTrigger
  def self.configuration
    @configuration ||= Configuration.new
  end

  def self.reset
    @configuration = Configuration.new
  end

  def self.configure
    yield(configuration)
  end
  VERSION = '0.1.0'
end
