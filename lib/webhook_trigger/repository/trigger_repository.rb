# frozen_string_literal: true

require 'mysql2'
module WebhookTrigger
  module Repository
    # This TriggerRepository is class for connect to database
    class TriggerRepository
      def self.client
        client = Mysql2::Client.new(
          host: WebhookTrigger.configuration.host,
          port: WebhookTrigger.configuration.port,
          username: WebhookTrigger.configuration.username,
          password: WebhookTrigger.configuration.password,
          database: WebhookTrigger.configuration.db
        )
        client
      end

      def self.webhook_list_query_select
        sql = 'SELECT ' + WebhookTrigger.configuration.list + '.id, '
        sql += WebhookTrigger.configuration.list + '.url, '
        sql += WebhookTrigger.configuration.event + '.name '
        sql += webhook_list_query_join
        sql
      end

      def self.webhook_list_query_join
        sql = ' FROM ' + WebhookTrigger.configuration.list
        sql += ' JOIN ' + WebhookTrigger.configuration.event
        sql += ' on ' + WebhookTrigger.configuration.list + '.event_id='
        sql += WebhookTrigger.configuration.event + '.id '
        sql
      end

      def self.webhook_list_query_condition(event, account_id)
        sql = 'WHERE ' + WebhookTrigger.configuration.list
        sql += '.account_id=' + account_id.to_s
        sql += ' AND ' + WebhookTrigger.configuration.event
        sql += ".name like '" + event.to_s + "'"
        sql
      end

      def self.webhook_list(event, account_id)
        sql = webhook_list_query_select
        sql += webhook_list_query_condition(event, account_id)
        results = client.query(sql)
        results
      end

      def self.webhook_insert_query
        sql = 'INSERT INTO ' + WebhookTrigger.configuration.db + '.'
        sql += WebhookTrigger.configuration.buffer_table
        sql += '(url, event, event_source_id) VALUES '
        sql
      end

      def self.webhook_insert_value(event, url, event_source_id)
        sql = "('" + url.to_s + "', '" + event.to_s + "', "
        sql += event_source_id.to_s + ')'
        sql
      end

      def self.webhook_insert_pending(event, url, event_source_id)
        sql = webhook_insert_query
        sql += webhook_insert_value(event, url, event_source_id)
        results = client.query(sql)
        results
      end
    end
  end
end
