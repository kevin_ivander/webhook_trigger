# frozen_string_literal: true

require 'webhook_trigger/version'
require 'webhook_trigger/trigger.rb'

module WebhookTrigger
  class Error < StandardError; end
  # Your code goes here...
end
