# frozen_string_literal: true

require 'webhook_trigger'
RSpec.shared_examples 'WebhookTriggerInitial' do
  include_examples 'WebhookTriggerFunc'
  let(:mysql_client) { double('Mysql2 Client', query: 1, sql: 'Query SQL') }
  let(:client) { double('client') }
  let(:configuration) { WebhookTrigger.configuration.new }

  subject do
    WebhookTrigger::Trigger
  end

  before(:all) do
    WebhookTrigger.configure do |config|
      config.db = 'test'
      config.host = 'test'
      config.port = 'test'
      config.username = 'test'
      config.password = 'test'
      config.buffer_table = 'test'
      config.list = 'test'
      config.event = 'test'
    end
  end

  before do
    allow(Mysql2::Client).to receive(
      :new
    ).and_return(mysql_client)
  end
end
