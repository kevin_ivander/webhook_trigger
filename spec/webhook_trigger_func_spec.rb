# frozen_string_literal: true

require 'webhook_trigger'
RSpec.shared_examples 'WebhookTriggerFunc' do
  def receive_get_webhooks
    expect(subject).to receive(:get_webhooks)
      .and_return(
        [
          {
            'id' => 1,
            'url' => 'https://google.com',
            'name' => 'account_activation'
          }
        ]
      )
  end

  def receive_get_webhooks_nil
    allow(subject).to receive(
      :get_webhooks
    ).and_return([])
  end

  def check_connection_pool
    trigger_repository = WebhookTrigger::Repository::TriggerRepository
    expect(trigger_repository.client).to eq(mysql_client)
  end
end
