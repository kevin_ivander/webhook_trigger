# frozen_string_literal: true

require 'webhook_trigger'
RSpec.describe WebhookTrigger do
  include_examples 'WebhookTriggerInitial'
  context 'when #perform is invoked' do
    it 'should check connection pool and webhook list' do
      trigger_repository = WebhookTrigger::Repository::TriggerRepository
      expect(trigger_repository.client).to eq(mysql_client)
      receive_get_webhooks
      subject.perform('account_activation', 1, 1)
    end
    context 'when webhook url is present' do
      context 'when iterate webhook list accurately' do
        context 'when insert into buffer tables' do
          it 'should return result' do
            receive_get_webhooks
            trigger = subject.perform('account_activation', 1, 1)
            expect(trigger.size).to eql(1)
          end

          it 'should return nil' do
            receive_get_webhooks_nil
            trigger = subject.perform('', 1, 1)
            expect(trigger.size).to eql(0)
          end
        end
      end
    end
  end
end
